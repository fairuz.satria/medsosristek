import React, { useEffect, useState } from 'react'
import { useNavigate } from "react-router-dom"
import Cookies from "universal-cookie"
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import PostinganCard from '../components/PostinganCard';
import { Row, Col, Modal, Button, Form } from 'react-bootstrap'
import axios from "axios";
import { useQuery } from '@tanstack/react-query'
import ModalComp from '../components/ModalComp'
import AddCFPopUp from '../components/AddCFPopUp'

const cookies = new Cookies();

function Main() {
  const [isi, setIsi] = useState("")
  const [forCloseFriend, setForCloseFriend] = useState(false)
  const [message, setModalMessage] = useState("")
  const [showModal, setShowModal] = useState(false);
  const [showCfModal, setShowCfModal] = useState(false);
  const [changed, setChanged] = useState(false);
  const token  = cookies.get("user").token
  const httpConf = {
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-Type': "application/json"
    }
  }

  const handleClose = () => setShowModal(false);
  const handleCfClose = () => setShowCfModal(false);

  const { data, refetch } = useQuery(["postingan"], () => {
    return axios.get("http://localhost:2020/api/postingan/get-postingan", httpConf).then((res) => res.data)
  })

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    const postData = {
      isi: isi,
      forCloseFriend: forCloseFriend
    };

    console.log(postData)

    try {
      const response = await axios.post("http://localhost:2020/api/postingan/add-postingan", postData, httpConf);

      if(response.status == 200){
        refetch()
        setModalMessage("Your content has been posted successfully");
        setShowModal(true)
      }
    }catch(error: any){
      setModalMessage("Something Wrong");
      setShowModal(true)
    }
  }

  const handleDelete = async (id: string) => {

    try {
      const response = await axios.post(`http://localhost:2020/api/postingan/delete-postingan/${id}`, {}, httpConf)

      if(response.status == 200){
        refetch()
        setModalMessage("Your content has been deleted")
        setShowModal(true)
      }
    }catch(error: any){
      setModalMessage("Something Wrong")
      setShowModal(true)
    }
  }
  
  const textareaStyle = { 
    width: "500px", 
    height: "250px",
    backgroundColor: "#08233f",
    color: "white",
    boxShadow: "0 0 5px #88BFE8",
    outline: "none",
    border: "none",
   }

   console.log("rebuild")

  return (
    <div className='container-fluid justify-content-center'>
        <Row className="justify-content-center">
          <Col lg={8} md={10} sm={12} xs={12} className="text-align-left" style={{ textAlign: 'left', color: "white", marginTop: "15px"}}>
            <strong  style={{fontSize: '2em'}}>Welcome Back,</strong>
          </Col>

          <Col lg={8} md={10} sm={12} xs={12} className="text-align-left" style={{ textAlign: 'left', color: "white", marginTop: "15px"}}>
            <strong  style={{fontSize: '1.5em', color: "#88BFE8"}}>@{cookies.get("user").username}</strong>
          </Col>

          <Col lg={8} md={10} sm={12} xs={12} className="text-align-left" style={{ textAlign: 'left', color: "white", marginTop: "15px"}}>
            <form method="post" onSubmit={handleSubmit} role="post">
            <div className="d-flex justify-content-between">
            <textarea style={textareaStyle} maxLength={400} name="isi" id="isi" placeholder="isi" value={isi} onChange={(event) => setIsi(event.target.value)} required className="form-control input-lg" />
              <Row className="align-items-end mt-auto me-auto">
                <Form.Check style={{ marginLeft: "20px" }} type="checkbox" name="forCloseFriend" id="forCloseFriend" label="For close friends" checked={forCloseFriend} onChange={(event) => {
                  console.log(event.target.checked)
                  return setForCloseFriend(event.target.checked)
                }} />
                <Button  style={{fontSize: '1em', marginLeft: "20px", backgroundColor: 'white', boxShadow: '2px 2px 5px rgba(255,255,255,1)'}} onClick={(event) => {
                  setShowCfModal(true)
                }} ><strong style={{color: "darkblue"}}>Add Close Friends</strong></Button>
                <Button type="submit" style={{ fontSize: '1em', marginTop: "10px", marginLeft: "20px", backgroundColor: 'white', boxShadow: '2px 2px 5px rgba(255,255,255,1)'}}><strong style={{color: "darkblue"}}>Post</strong></Button>
              </Row>
            </div>
            </form>
          </Col>
          
          {data?.map((postingan: any) => (
            <PostinganCard username={String(postingan.username)} id={String(postingan.id)} date={String(postingan.createdAt)} handleDelete={handleDelete} postContent={String(postingan.isi)} photoUrl={postingan.photoUrl===""?'https://uxwing.com/wp-content/themes/uxwing/download/peoples-avatars/no-profile-picture-icon.png':String(postingan.photoUrl)}></PostinganCard>
          ))}

        </Row>
        <ModalComp handleClose={handleClose} showModal={showModal} message={message} />
        <AddCFPopUp show={showCfModal} onHide={handleCfClose} refetch={refetch} setModalMessage={setModalMessage} setShowModal={setShowModal}/>
    </div>
  )
}

export default Main