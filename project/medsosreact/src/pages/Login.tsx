import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import jwt from 'jwt-decode';
import Cookies from "universal-cookie";
import "../assets/css/styles.css"
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import { Button, Modal } from "react-bootstrap";
import { login, logout } from "../redux/userStore";
import { useDispatch } from "react-redux";
import ModalComp from "../components/ModalComp";

const cookies = new Cookies();

const Login = () => {
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [message, setModalMessage] = useState("")
  const [showModal, setShowModal] = useState(false);

  const navigate = useNavigate()

  const dispatch = useDispatch()

  const handleClose = () => setShowModal(false);

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    const authData = {
      username: username,
      password: password,
    };

    try {
      const response = await axios.post("http://localhost:2020/api/auth/signin", authData);

      if(response.status == 200){
        const user = response.data

        const jwtToken = response.data.token
        const decodedJwt: any = jwt(jwtToken)
        console.log(decodedJwt)
        
        cookies.set("user", user, {
            expires: new Date(decodedJwt.exp * 1000),
        });

        navigate("/")
        }else{
          setModalMessage("Login failed. Please try again.");
          setShowModal(true)
          console.log(response.status)
        }

    } catch (error: any) {
      if(error.response?.status === 401){
        setModalMessage("Username atau password salah");
        setShowModal(true)
      }else{
          setModalMessage("Login failed try again");
          setShowModal(true)
      }
      console.error(error);
    }
  };

  return (
    // <div className="container">
    //   <form onSubmit={handleSubmit}>
    //     <div className="form-group">
    //       <label htmlFor="username">Username</label>
    //       <input
    //         type="text"
    //         className="form-control"
    //         id="username"
    //         value={username}
    //         onChange={(event) => setUsername(event.target.value)}
    //       />
    //     </div>
    //     <div className="form-group">
    //       <label htmlFor="password">Password</label>
    //       <input
    //         type="password"
    //         className="form-control"
    //         id="password"
 //            value={password}
 //            onChange={(event) => setPassword(event.target.value)}
    //       />
    //     </div>
    //     <button type="submit" className="btn btn-primary">
    //       Login
    //     </button>
    //   </form>
    // </div>
    <div>
			<div className="login-form">
				<form method="post" onSubmit={handleSubmit} role="login">
					<img src="https://avatars.githubusercontent.com/u/3433898?s=200&v=4" className="img-responsive" alt="" style={{width: "10%"}} />
					<input type="username" name="username" id="username" placeholder="Username" value={username} onChange={(event) => setUsername(event.target.value)}required className="form-control input-lg" />
					<input type="password" name="password" id="password" placeholder="Password" value={password} onChange={(event) => setPassword(event.target.value)}required className="form-control input-lg" />
					<button type="submit" name="go" className="btn btn-lg btn-primary btn-block btn-expand">Sign in</button>
					<div>
						<a style={{color: "#01162D"}} href="/register">Create account</a>
					</div>
				</form>
			</div>
      <ModalComp handleClose={handleClose} showModal={showModal} message={message} />
  </div>

  

  );
};

export default Login;
