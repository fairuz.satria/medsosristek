import { useQuery } from '@tanstack/react-query';
import axios from 'axios';
import React, { useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import Cookies from 'universal-cookie';
import ModalComp from '../components/ModalComp';
import PostinganCard from '../components/PostinganCard'

const cookies = new Cookies();

const ProfilePage = () => {
  const [message, setModalMessage] = useState("")
  const [showModal, setShowModal] = useState(false);
  const token  = cookies.get("user").token
  const httpConf = {
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-Type': "application/json"
    }
  }

  const handleClose = () => setShowModal(false);

  const { data, refetch } = useQuery(["mypostingan"], () => {
    return axios.get(`http://localhost:2020/api/user/${cookies.get("user").username}/profile`, httpConf).then((res) => {
      console.log(res.data)
      return res.data
    })
  })

  const handleDelete = async (id: string) => {

    try {
      const response = await axios.post(`http://localhost:2020/api/postingan/delete-postingan/${id}`, {}, httpConf)

      if(response.status == 200){
        refetch()
        setModalMessage("Your content has been deleted")
        setShowModal(true)
      }
    }catch(error: any){
      setModalMessage("Something Wrong")
      setShowModal(true)
    }
  }
  return (
    <div className='container-fluid justify-content-center'>
      <Row className="justify-content-center">
        {/* <PostinganCard username="johm" date="29-12-2020" postContent='Hello Ristek' photoUrl='https://uxwing.com/wp-content/themes/uxwing/download/peoples-avatars/no-profile-picture-icon.png' id= handleDelete={handleDelete}></PostinganCard> */}
          <Col lg={8} md={10} sm={12} xs={12} className="text-align-left" style={{ textAlign: 'left', color: "white", marginTop: "15px"}}>
            <img style={{width: "75px", height: "75px", borderRadius: "50%"}} src={cookies.get("user").photoUrl===""?"https://uxwing.com/wp-content/themes/uxwing/download/peoples-avatars/no-profile-picture-icon.png":cookies.get("user").photoUrl}/>
            <strong  style={{fontSize: '2em', color: "#88BFE8", marginLeft:  "15px"}}>@{cookies.get("user").username}</strong>
          </Col>

          <Col lg={8} md={10} sm={12} xs={12} className="text-align-left" style={{ textAlign: 'left', color: "white", marginTop: "15px"}}>
            <p>{cookies.get("user").bio}</p>
          </Col>
          {data?.listPostingan?.map(((postingan: any) => (
            <PostinganCard username={String(postingan.username)} id={String(postingan.id)} date={String(postingan.createdAt)} handleDelete={handleDelete} postContent={String(postingan.isi)} photoUrl={postingan.photoUrl===""?'https://uxwing.com/wp-content/themes/uxwing/download/peoples-avatars/no-profile-picture-icon.png':String(postingan.photoUrl)}></PostinganCard>
          )))}
      </Row>
      <ModalComp handleClose={handleClose} showModal={showModal} message={message} />
    </div>
  )
}

export default ProfilePage