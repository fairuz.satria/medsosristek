import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import jwt from 'jwt-decode';
import Cookies from "universal-cookie";
import "../assets/css/styles.css"
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import { Button, Modal } from "react-bootstrap";

const cookies = new Cookies();

const Register = () => {
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [email, setEmail] = useState("")
  const [message, setModalMessage] = useState("")
  const [showModal, setShowModal] = useState(false);
  
  const navigate = useNavigate()

  const handleClose = () => setShowModal(false);

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    const authData = {
      email: email,
      username: username,
      password: password,
    };

    try {
      const response = await axios.post("http://localhost:2020/api/auth/signup", authData);

      if(response.status == 200){
        // const jwtToken = response.data.token
        // const decodedJwt: any = jwt(jwtToken)
        // console.log(decodedJwt)
        
        // cookies.set("jwt", jwtToken, {
        //     expires: new Date(decodedJwt.exp * 1000),
        // });
        // navigate("/")
          setModalMessage("User successfuly registered");
          setShowModal(true)
          console.log(response.status)
        }else{
          
            console.log(response.status)
        }

    } catch (error: any) {
      setModalMessage(error.response.data.message);
      setShowModal(true)
      console.error(error);
    }
  };

  return (
    // <div className="container">
    //   <form onSubmit={handleSubmit}>
    //     <div className="form-group">
    //       <label htmlFor="username">Username</label>
    //       <input
    //         type="text"
    //         className="form-control"
    //         id="username"
    //         value={username}
    //         onChange={(event) => setUsername(event.target.value)}
    //       />
    //     </div>
    //     <div className="form-group">
    //       <label htmlFor="password">Password</label>
    //       <input
    //         type="password"
    //         className="form-control"
    //         id="password"
 //            value={password}
 //            onChange={(event) => setPassword(event.target.value)}
    //       />
    //     </div>
    //     <button type="submit" className="btn btn-primary">
    //       Login
    //     </button>
    //   </form>
    // </div>
    	<div>
        <div className="login-form">
          <form method="post" onSubmit={handleSubmit} role="login">
            <img src="https://avatars.githubusercontent.com/u/3433898?s=200&v=4" className="img-responsive" alt="" style={{width: "10%"}} />

            <input type="email" name="email" id="email" placeholder="Email" value={email} onChange={(event) => setEmail(event.target.value)}required className="form-control input-lg" />
            <input type="username" name="username" id="username" placeholder="Username" value={username} onChange={(event) => setUsername(event.target.value)}required className="form-control input-lg" />
            <input type="password" name="password" id="password" placeholder="Password" value={password} onChange={(event) => setPassword(event.target.value)}required className="form-control input-lg" />
            
            <button type="submit" name="go" className="btn btn-lg btn-primary btn-block btn-expand">Sign up</button>
            <div>
              <a href="/login">Login</a>
            </div>
          </form>
        </div>
        <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Register Error</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          {message=="User successfuly registered"?(
            <Button variant="primary" href="/login">
            Login
            </Button>
          ) : <></>}
        </Modal.Footer>
      </Modal>
  </div>
  );
};

export default Register;
