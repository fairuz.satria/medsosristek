import React from 'react';
import { BrowserRouter as Router, Route, Routes, useLocation} from 'react-router-dom';
import './App.css';
import Navbar from './components/Navbar';
import Login from './pages/Login';
import Main from './pages/Main';
import Register from './pages/Register';
import RouteAuthenticatedPage from './routes/RouteAuthenticatedPage';
import { Provider } from 'react-redux';
import userStore from './redux/userStore';
import { QueryClientProvider, QueryClient } from '@tanstack/react-query';

function App() {
    
  const client = new QueryClient();
  return (
    <div className="App">
      <QueryClientProvider client={client}>
      <Provider store={userStore}>
        <Router>
          <Routes>
            <Route path="/*" element={<RouteAuthenticatedPage />}></Route>
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
          </Routes>
        </Router>
      </Provider>
      </QueryClientProvider>
    </div>
  );
}

export default App;
