import React from 'react'
import { configureStore, createSlice } from '@reduxjs/toolkit'

const userSlice = createSlice({
    name: "user",
    initialState: {value:{username: "", photoUrl: "", email: ""}},
    reducers: {
        login: (state, action) => {
            state.value = action.payload
        },

        logout: (state) => {
            state.value = {username: "", photoUrl: "", email: ""}
        }
    }
})

const userStore = configureStore({
    reducer: userSlice.reducer,
});

export const {login, logout} = userSlice.actions
export default userStore
