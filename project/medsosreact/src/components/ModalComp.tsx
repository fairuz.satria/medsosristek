import React from 'react'
import { Row, Col, Modal, Button, Form } from 'react-bootstrap'

interface Props {
  handleClose: () => void;
  message: string,
  showModal: boolean
}

const ModalComp = (props: Props) => {
  return (
    <Modal show={props.showModal} onHide={props.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Posting Status</Modal.Title>
          </Modal.Header>
          <Modal.Body>{props.message}</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={props.handleClose}>
              Close
            </Button>
          </Modal.Footer>
    </Modal>
  )
}

export default ModalComp