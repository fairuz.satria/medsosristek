import React from 'react';
import { Navbar, Nav, NavDropdown, Image } from 'react-bootstrap';
import { useNavigate } from "react-router-dom"
import { Cookies } from 'react-cookie';


const cookies = new Cookies();

const NavbarComponent = () => {

  const navigate = useNavigate()

  const handleLogout = () =>{
      cookies.remove("user")
      navigate("/login")
  }

  const navbarConfig = {
      preventOverflow: {
      enabled: true,
      boundariesElement: 'scrollParent'
    }
  }

  return (
    
    <Navbar expand="lg" style={{backgroundColor: "#022346"}}>
      <Navbar.Brand href="/">
        <img
          src={"https://avatars.githubusercontent.com/u/3433898?s=200&v=4"}
          width="30"
          height="30"
          className="d-inline-block align-top"
          alt="Brand Logo"
        />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav" >
        <Nav className="ms-auto"></Nav>
        <Nav>
          <NavDropdown title={<Image style={{width: "30px", height: "30px"}} src={cookies.get("user").photoUrl===""?"https://uxwing.com/wp-content/themes/uxwing/download/peoples-avatars/no-profile-picture-icon.png":cookies.get("user").photoUrl} roundedCircle />} id="basic-nav-dropdown" className="dropdown-menu-left" drop="start">
            <NavDropdown.Item href="/profile">{cookies.get("user").username}</NavDropdown.Item>
            <NavDropdown.Item>Account Settings</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item onClick={handleLogout}>Log Out</NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavbarComponent;