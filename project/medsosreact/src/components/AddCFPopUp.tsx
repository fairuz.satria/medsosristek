import { useQuery } from '@tanstack/react-query'
import axios from 'axios'
import React, { ChangeEvent, useEffect, useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import Cookies from 'universal-cookie'

const cookies = new Cookies()

interface User {
  username: string,
  isCloseFriend: Boolean
}

interface CfProps {
  refetch: any,
  setModalMessage: any,
  setShowModal: any,
  show: boolean, 
  onHide: any 
}

const AddCFPopUp = ({refetch, setModalMessage, setShowModal, show, onHide}: CfProps) => {
  // const { data, refetch: cfusersrefetch} = useQuery(["cfusers"], () => {
  //   return axios.get("http://localhost:2020/api/user/get-all-user", httpConf).then(res => {
  //     const mappedUsers = res.data.map((user: any) => ({
  //       username: user.username,
  //       isCloseFriend: user.isCloseFriend
  //     }));
  //     setUserNext(mappedUsers)
  //     return mappedUsers
  //   });
  // })



  const [usersFirst, setUserNext] = useState<User[]>([]);
  const token  = cookies.get("user").token
  const httpConf = {
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-Type': "application/json"
    }
  }

  useEffect(() => {
    axios.get('http://localhost:2020/api/user/get-all-user', httpConf)
      .then(response => {
        const mappedUsers = response.data.map((user: any) => ({
          username: user.username,
          isCloseFriend: user.isCloseFriend
        }));
        setUserNext(mappedUsers);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);



  const handleCheckboxChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value, checked } = event.target;
    setUserNext(users => {
      const updatedUsers = [...users];
      updatedUsers[+value] = { ...updatedUsers[+value], isCloseFriend: checked };
      return updatedUsers;
    });
  }

  

  const handleSubmit = async () => {
    try {
      const response = await axios.post(`http://localhost:2020/api/user/add-close-friend`, {"listUser": usersFirst}, httpConf)

      if(response.status == 200){
        refetch()
        setModalMessage("Your list of close friends has been updated")
        setShowModal(true)
      }
    }catch(error: any){
      console.log(error)
      setModalMessage("Something went wrong")
      setShowModal(true)
    }
  }
  return (
    <Modal show={show} onHide={onHide}>
    <Modal.Header closeButton>
      <Modal.Title>Select close friends</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <Form onSubmit={handleSubmit}>
        {usersFirst?.map((user: any, index: number) => {
          console.log(user)
          return(<Form.Check
            key={user.username}
            type="checkbox"
            label={user.username}
            value={index}
            checked={user.isCloseFriend}
            onChange={handleCheckboxChange}
          />)
        })}
        <Button type="submit">Save</Button>
      </Form>
    </Modal.Body>
  </Modal>
  )
}

export default AddCFPopUp