import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import { Card, Image, Row, Col, Button } from "react-bootstrap"
import "../assets/css/styles.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import Cookies from 'universal-cookie'

const cookies = new Cookies()

interface PostinganCardProps { 
    id: string
    username: string,
    date: string, 
    postContent: string,
    photoUrl: string
    handleDelete: (id: string) => Promise<void>
}

const PostinganCard = ({username, date, postContent, photoUrl, id, handleDelete}: PostinganCardProps) => {
  const user = cookies.get("user")
    return (
      <Col lg={8} md={10} sm={12} xs={12} className="justify-content-center">
        <Card text="white" className="post-card shadow" style={{ background: 'linear-gradient(to bottom right,  #022346, #010911)', paddingTop: "15px", marginTop: "25px"}}>
          <Card.Header style={{ textAlign: 'left', display: 'flex', justifyContent: 'space-between', alignItems: 'center', }} >
            <div>
              <Image  src={photoUrl} roundedCircle width={30} height={30} className="mr-2" />
              <strong style={{paddingLeft: "10px"}}>{username}</strong>
              <small style={{paddingLeft: "10px"}} className="text-muted ml-2">{date}</small>
            </div>
            {String(user.username)===username?(
              <Button variant="outline-light" onClick={() => handleDelete(id)}>
              <FontAwesomeIcon icon={faTrash} />
            </Button>
            ):<></>}
          </Card.Header>
          <Card.Body>
            <Card.Text style={{ textAlign: 'left' }}>{postContent}</Card.Text>
          </Card.Body>
        </Card>
        </Col>
    //     <Card className="shadow rounded mb-3">
    //     <Card.Body>
    //       <Row>
    //         <Col sm={2}>
    //           <div className="d-flex justify-content-center">
    //             <div className="rounded-circle overflow-hidden border border-3 border-white">
    //               <img
    //                 className="w-100 h-100"
    //                 src={photoUrl}
    //                 alt={`Profile of ${username}`}
    //               />
    //             </div>
    //           </div>
    //         </Col>
    //         <Col sm={10}>
    //           <div className="d-flex justify-content-between align-items-center mb-2">
    //             <Card.Title className="mb-0">{username}</Card.Title>
    //             <small className="text-muted">{date}</small>
    //           </div>
    //           <Card.Text className="text-justify">{postContent}</Card.Text>
    //         </Col>
    //       </Row>
    //     </Card.Body>
    //   </Card>
      );
}

export default PostinganCard