import { BrowserRouter as Router, Route, Routes, useLocation} from 'react-router-dom';
import Navbar from '../components/Navbar';
import Main from '../pages/Main';
import React, { useEffect } from 'react'
import { useNavigate } from "react-router-dom"
import Cookies from "universal-cookie"
import ProfilePage from '../pages/ProfilePage';

const cookies = new Cookies();

const RouteAuthenticatedPage = () => {

  const navigate = useNavigate()

  useEffect(() => {
    if(cookies.get("user") === undefined){
      navigate("/login")
      
      console.log("in")
    }
    // const decodedUser = decodeURI(cookies.get("user"))
    // console.log(cookies.get("user").email)
  })

  if(!(cookies.get("user") === undefined)){
    return (
      <div className='container-fill'>
        <Navbar />
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/profile" element={<ProfilePage />} />
        </Routes>
      </div>
    )
  }
  return <></>
}

export default RouteAuthenticatedPage;