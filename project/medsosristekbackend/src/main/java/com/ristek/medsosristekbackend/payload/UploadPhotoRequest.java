package com.ristek.medsosristekbackend.payload;

import lombok.Data;

@Data
public class UploadPhotoRequest {
    String photoURL;
}
