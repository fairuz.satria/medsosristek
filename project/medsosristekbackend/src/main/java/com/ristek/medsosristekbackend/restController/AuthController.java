package com.ristek.medsosristekbackend.restController;

import com.ristek.medsosristekbackend.model.UserMedsosModel;
import com.ristek.medsosristekbackend.payload.JwtResponse;
import com.ristek.medsosristekbackend.payload.LoginRequest;
import com.ristek.medsosristekbackend.payload.MessageResponse;
import com.ristek.medsosristekbackend.payload.RegisterRequest;
import com.ristek.medsosristekbackend.security.jwt.JwtUtils;
import com.ristek.medsosristekbackend.security.services.UserDetailsImpl;
import com.ristek.medsosristekbackend.service.UserMedsosRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600, allowedHeaders = "*")
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UserMedsosRestService userRestService;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        userRestService.setAuthentication(authentication);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        // List<String> roles = userDetails.getAuthorities().stream()
        //         .map(item -> item.getAuthority())
        //         .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getUuid(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                userDetails.getPhotoUrl(),
                userDetails.getBio()));
    }
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterRequest registerRequest) {
        if (userRestService.existsByUsername(registerRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Username sudah digunakan!"));
        }
        if (userRestService.existsByEmail(registerRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Email sudah digunakan!"));
        }

        // Create new user's account
        UserMedsosModel addUser = new UserMedsosModel();
        addUser.setUsername(registerRequest.getUsername());
        addUser.setPassword(registerRequest.getPassword());
        addUser.setEmail(registerRequest.getEmail());
//        addUser.setIsSso(false);
        userRestService.addUser(addUser);
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}
