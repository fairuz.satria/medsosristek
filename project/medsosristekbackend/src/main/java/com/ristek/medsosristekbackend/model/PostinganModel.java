package com.ristek.medsosristekbackend.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "postingan")
public class PostinganModel implements Serializable{
    @Id
    @GeneratedValue
    @Range(max = 20)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "isi")
    private String isi = "";

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "id_postingan", referencedColumnName = "uuidUserMedsos", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private UserMedsosModel userMedsos;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime createdAt;

    @Column(name="for_close_friend")
    private Boolean forCloseFriend;

    @JsonProperty("username")
    public String getUsername() {
        return userMedsos.getUsername();
    }

    @JsonProperty("photoUrl")
    public String getPhotoUrl() {
        return userMedsos.getPhotoUrl();
    }
}