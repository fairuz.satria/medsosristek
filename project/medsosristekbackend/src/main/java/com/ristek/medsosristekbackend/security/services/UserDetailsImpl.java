package com.ristek.medsosristekbackend.security.services;


import com.ristek.medsosristekbackend.model.UserMedsosModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserDetailsImpl implements UserDetails {

    private static final long serialVersionUID = 1L;

    private String uuid;

    private String username;

    private String email;

    private String photoUrl;

    private String bio;

    @JsonIgnore
    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    public UserDetailsImpl(String uuid, String username, String email, String password, String photoUrl, String bio) {
        this.uuid = uuid;
        this.username = username;
        this.email = email;
        this.password = password;
        this.photoUrl = photoUrl;
        this.bio = bio;
    }

    public static UserDetailsImpl build(UserMedsosModel userModel) {
        // List<String> roles = new ArrayList<>();
        // roles.add(userModel.getRole());
        // List<GrantedAuthority> authorities = roles.stream()
        //         .map(role -> new SimpleGrantedAuthority(role))
        //         .collect(Collectors.toList());
        return new UserDetailsImpl(
                userModel.getUuidUserMedsos(),
                userModel.getUsername(),
                userModel.getEmail(),
                userModel.getPassword(),
                userModel.getPhotoUrl(),
                userModel.getBio()  
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public String getUuid(){
        return uuid;
    }
    public String getEmail(){
        return email;
    }

    public String getBio(){
        return bio;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(uuid, user.uuid);
    }

}
