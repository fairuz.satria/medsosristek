package com.ristek.medsosristekbackend.payload;

import lombok.Data;

@Data
public class DeletePostReq {
    String id;
}
