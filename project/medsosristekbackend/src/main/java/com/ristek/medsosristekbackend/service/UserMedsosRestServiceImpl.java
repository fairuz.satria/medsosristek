package com.ristek.medsosristekbackend.service;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ristek.medsosristekbackend.model.UserMedsosModel;
import com.ristek.medsosristekbackend.model.PostinganModel;
import com.ristek.medsosristekbackend.repository.UserMedsosDb;
import com.ristek.medsosristekbackend.security.services.UserDetailsImpl;



@Service
@Transactional
public class UserMedsosRestServiceImpl implements UserMedsosRestService{

    @Autowired
    private UserMedsosDb userDb;

    private Authentication userAuthentication;

    @Override
    public UserMedsosModel addUser(UserMedsosModel user){
        user.setPassword(encrypt(user.getPassword()));
        return userDb.save(user);
    }
    
    @Override
    public List<UserMedsosModel> retrieveListUser(){
        return userDb.findAll();
    }

    @Override
    public boolean existsByUsername(String username) {
        return userDb.existsByUsername(username);
    }

    @Override
    public boolean existsByEmail(String email) {
        return userDb.existsByEmail(email);
    }


    @Override
    public String encrypt(String password) {
      BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
      String hashedPassword = passwordEncoder.encode(password);
      return hashedPassword;
    }

    @Override
    public Authentication getAuthentication(){
        return this.userAuthentication;
    }

    @Override
    public void setAuthentication(Authentication newAuthentication){
        this.userAuthentication = newAuthentication;
    }

    @Override
    public boolean isCloseFriends(String usernameC, String username){
        UserMedsosModel userLogedIn = userDb.findByUsername(username).get();
        Set<UserMedsosModel> closeFriends = userLogedIn.getCloseFriends();

        for(UserMedsosModel userCheck : closeFriends){
            if(usernameC.equals(userCheck.getUsername())){
                return true;
            }
        }

        return false;
    }

    @Override
    public UserMedsosModel addOrUpdateImage(String photoUrl, String username){
        UserMedsosModel user = userDb.findByUsername(username).get();
        user.setPhotoUrl(photoUrl);
        return userDb.save(user);
    }

    @Override
    public UserMedsosModel updateProfilBio(String newBio, String username){
        UserMedsosModel user = userDb.findByUsername(username).get();
        user.setBio(newBio);
        return userDb.save(user);
    }

    @Override
    public Boolean updateCloseFriends(String username, List<HashMap<String, String>> objAllUser){
        UserMedsosModel user = userDb.findByUsername(username).get();
        for(HashMap<String,String> obj : objAllUser){
            if(obj.get("isCloseFriend").equals("true")){
                addCloseFriend(obj.get("username"), user);
            }else{
                unFollow(obj.get("username"), user);
            }
        }
        return true;

    }

    @Override
    public void addCloseFriend(String usernameC, UserMedsosModel user){
        UserMedsosModel userC = userDb.findByUsername(usernameC).get();
        Set<UserMedsosModel> listCloseFriends = user.getCloseFriends();
        Set<UserMedsosModel> listFollowed = userC.getFollowedBy();

        if(listCloseFriends == null){
            listCloseFriends = new HashSet<>();
        }

        listCloseFriends.add(userC);
        listFollowed.add(user);

        user.setCloseFriends(listCloseFriends);
        userC.setFollowedBy(listFollowed);

        userDb.save(user);
        userDb.save(userC);
    }

    @Override
    public void unFollow(String usernameC, UserMedsosModel user){
        UserMedsosModel userC = userDb.findByUsername(usernameC).get();
        Set<UserMedsosModel> listCloseFriends = user.getCloseFriends();
        Set<UserMedsosModel> listFollowed = userC.getFollowedBy();

        listCloseFriends.remove(userC);
        listFollowed.remove(user);

        user.setCloseFriends(listCloseFriends);
        userC.setFollowedBy(listFollowed);

        userDb.save(user);
        userDb.save(userC);
    }

    @Override
    public List<UserMedsosModel> getAllUser(){
        return userDb.findAll();
    }

    @Override
    public List<UserMedsosModel> getAllUserTaggedCf(String username){
        List<UserMedsosModel> allUser = userDb.findAll();
        List<UserMedsosModel> newAllUser = new ArrayList<>();

        for(UserMedsosModel user : allUser){
            String usernameC = user.getUsername();
            if(!(usernameC.equals(username))){
                user.setIsCloseFriend(isCloseFriends(usernameC, username));
                newAllUser.add(user);
            }
        }

        return newAllUser;
    }

    // @Override
    // public List<UserMedsosModel> getAllUserNotCloseFriend(String username){
    //     List<UserMedsosModel> allUser = userDb.findAll();
    //     List<UserMedsosModel> userNotCloseFriends = new ArrayList<>();

    //     for(UserMedsosModel user : allUser){
    //         String usernameC = user.getUsername();
    //         if(!isCloseFriends(usernameC, username) && !usernameC.equals(username)){
    //             userNotCloseFriends.add(user);
    //         }
    //     }

    //     return userNotCloseFriends;
    // }

    @Override
    public Set<UserMedsosModel> getCloseFriends(String username){
        UserMedsosModel user = userDb.findByUsername(username).get();
        return user.getCloseFriends();
    }

    @Override
    public UserMedsosModel getUser(String username){
        return userDb.findByUsername(username).get();
    }

}
