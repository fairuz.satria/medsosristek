package com.ristek.medsosristekbackend.service;

import com.ristek.medsosristekbackend.model.PostinganModel;
import com.ristek.medsosristekbackend.model.UserMedsosModel;
import com.ristek.medsosristekbackend.payload.NewPostinganRequest;
import com.ristek.medsosristekbackend.repository.PostinganDb;
import com.ristek.medsosristekbackend.repository.UserMedsosDb;

import javax.transaction.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class PostinganRestServiceImpl implements PostinganRestService{

    @Autowired
    private PostinganDb postingDb;

    @Autowired
    private UserMedsosDb userDb;

    @Autowired
    private UserMedsosRestService userService;

    @Override
    public PostinganModel addPostingan(NewPostinganRequest newPostingReq, String username) {
        UserMedsosModel user = userDb.findByUsername(username).get();
        LocalDateTime now = LocalDateTime.now();
        PostinganModel newPosting = new PostinganModel();

        newPosting.setCreatedAt(now);
        newPosting.setUserMedsos(user);
        newPosting.setForCloseFriend(newPostingReq.getForCloseFriend());
        newPosting.setIsi(newPostingReq.getIsi());

        return postingDb.save(newPosting);
    }

    @Override
    public PostinganModel updatePostingan(UserMedsosModel user, String isiBaru, Long id){
        List<PostinganModel> listPostingan = user.getListPostingan();

        for(PostinganModel post : listPostingan){
            if(post.getId() == id){
                post.setIsi(isiBaru);
                //postingDb.save(post);
                user.setListPostingan(listPostingan);
                userDb.save(user);
                return post;
            }
        }  

        return null;

    }

    @Override
    public PostinganModel getPostinganById(Long id){
        return postingDb.findById(id).get();
    }

    @Override
    public List<PostinganModel> getAllPostingForUser(String username){
        List<UserMedsosModel> allUser = userDb.findAll();
        List<PostinganModel> allEligablePosting = new ArrayList<>();

        for(UserMedsosModel user : allUser){
            if(user.getUsername().equals(username)){
                allEligablePosting.addAll(user.getListPostingan());
                continue;
            }
            allEligablePosting.addAll(getAllOtherPostingan(user.getUsername(), username));
        }

        return allEligablePosting;
    }

    
    @Override
    public List<PostinganModel> getAllOtherPostingan(String usernameC, String username){
        UserMedsosModel userGetted = userDb.findByUsername(usernameC).get();
        
        List<PostinganModel> postinganGetted = userGetted.getListPostingan();
        if(userService.isCloseFriends(usernameC, username)){
            return postinganGetted;
        }
        return filterPostingan(postinganGetted);
    }

    @Override
    public void deletePostingan(Long id){
        postingDb.deleteById(id);
    }

    public List<PostinganModel> filterPostingan(List<PostinganModel> listPostingan){
        List<PostinganModel> filtered = new ArrayList<>();

        for(PostinganModel posting : listPostingan){
            if(!posting.getForCloseFriend()){
                filtered.add(posting);
            }
        }

        return filtered;
    }
}
