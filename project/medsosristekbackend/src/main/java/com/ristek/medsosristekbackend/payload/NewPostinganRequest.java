package com.ristek.medsosristekbackend.payload;

import lombok.Data;

@Data
public class NewPostinganRequest {
    private String isi;
    private Boolean forCloseFriend;
}
