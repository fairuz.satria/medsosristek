package com.ristek.medsosristekbackend.payload;

import lombok.Data;

import java.util.List;

@Data
public class JwtResponse {
    private String token;
    private String uuid;
    private String username;
    private String email;
    private String photoUrl;
    private String bio;

    public JwtResponse(String token, String uuid, String username, String email, String photoUrl, String bio) {
        this.token = token;
        this.uuid = uuid;
        this.username = username;
        this.email = email;
        this.photoUrl = photoUrl;
        this.bio = bio;
    }
}
