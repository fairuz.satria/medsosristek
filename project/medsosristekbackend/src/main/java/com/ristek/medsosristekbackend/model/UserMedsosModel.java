package com.ristek.medsosristekbackend.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonIdentityInfo(
   generator = ObjectIdGenerators.PropertyGenerator.class,
   property = "username")
@Table(name = "usermedsos")
public class UserMedsosModel {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Size(max = 255)
    private String uuidUserMedsos;
   
    @NotNull
    @Size(max = 255)
    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @NotNull
    @Size(max = 255)
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @JsonIgnore
    @NotNull
    @Size(max = 255)
    @Column(name = "password", nullable = false)
    private String password;

    @Size(max = 255)
    @Column(name = "photo_url")
    private String photoUrl = "";

    @Column(name = "bio")
    private String bio = "";

    @OneToMany(mappedBy = "userMedsos", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PostinganModel> listPostingan;

    @ManyToMany
    private Set<UserMedsosModel> closeFriends = new HashSet<>();
 
    @JsonIgnore
    @ManyToMany(mappedBy = "closeFriends")
    private Set<UserMedsosModel> followedBy = new HashSet<>();

    @Transient
    private Boolean isCloseFriend;

    @Transient
    private Boolean isCfUpdated = false;
}

