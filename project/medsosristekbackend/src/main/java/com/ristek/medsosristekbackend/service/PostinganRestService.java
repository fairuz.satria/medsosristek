package com.ristek.medsosristekbackend.service;


import java.util.List;

import com.ristek.medsosristekbackend.model.PostinganModel;
import com.ristek.medsosristekbackend.model.UserMedsosModel;
import com.ristek.medsosristekbackend.payload.NewPostinganRequest;


public interface PostinganRestService {
    PostinganModel addPostingan(NewPostinganRequest newPosting, String user);
    PostinganModel updatePostingan(UserMedsosModel user, String isiBaru, Long id);
    List<PostinganModel> getAllPostingForUser(String username);
    List<PostinganModel> getAllOtherPostingan(String usernameC, String username);
    PostinganModel getPostinganById(Long id);
    void deletePostingan(Long id);
}
