package com.ristek.medsosristekbackend.payload;

import lombok.Data;

@Data
public class UpdateBioRequest {
    String bio;
}
