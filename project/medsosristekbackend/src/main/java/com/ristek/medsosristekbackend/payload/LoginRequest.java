package com.ristek.medsosristekbackend.payload;

import lombok.Data;

@Data
public class LoginRequest {
    private String username;
    private String password;
}
