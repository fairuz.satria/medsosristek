package com.ristek.medsosristekbackend.payload;

import java.util.HashMap;
import java.util.List;

import lombok.Data;

@Data
public class UpdateCloseFriendRequest {
    List<HashMap<String, String>> listUser;
}
