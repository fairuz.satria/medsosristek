package com.ristek.medsosristekbackend.restController;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.ristek.medsosristekbackend.payload.MessageResponse;
import org.springframework.http.ResponseEntity;

import com.ristek.medsosristekbackend.payload.NewPostinganRequest;
import com.ristek.medsosristekbackend.security.services.UserDetailsImpl;
import com.ristek.medsosristekbackend.service.PostinganRestService;
import com.ristek.medsosristekbackend.service.UserMedsosRestService;
import com.ristek.medsosristekbackend.model.PostinganModel;
import com.ristek.medsosristekbackend.model.UserMedsosModel;

@RestController
@RequestMapping("/api/postingan")
public class PostinganController {

    @Autowired
    private PostinganRestService postingService;

    @Autowired
    private UserMedsosRestService userRestService;

    @CrossOrigin
    @PostMapping(value = "/add-postingan")
    private PostinganModel addPostingan(@Valid @RequestBody NewPostinganRequest appointmentRequest){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
        return postingService.addPostingan(appointmentRequest, userDetails.getUsername());
    }

    @PostMapping(value = "/{usernameC}/postingan")
    private ResponseEntity<?> getPostingan(@PathVariable String usernameC){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
        return ResponseEntity.ok(postingService.getAllOtherPostingan(usernameC, userDetails.getUsername()));
    }

    @CrossOrigin
    @GetMapping(value = "/get-postingan")
    private ResponseEntity<?> getAllPostingan(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();

        return ResponseEntity.ok(postingService.getAllPostingForUser(userDetails.getUsername()));
    }

    @CrossOrigin
    @PostMapping(value = "/delete-postingan/{id}")
    private ResponseEntity<?> deletePostingan(@PathVariable Long id){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();

        PostinganModel postingan = postingService.getPostinganById(id);

        if(postingan.getUserMedsos().getUuidUserMedsos().equals(userDetails.getUuid())){
            postingService.deletePostingan(id);
            return ResponseEntity.ok(new MessageResponse("Posted deleted successfully"));
        }
        return ResponseEntity.badRequest().build();
    }
}
