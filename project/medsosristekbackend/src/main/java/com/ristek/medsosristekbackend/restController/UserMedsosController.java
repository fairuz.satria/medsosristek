package com.ristek.medsosristekbackend.restController;

import org.apache.catalina.connector.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ristek.medsosristekbackend.payload.UpdateBioRequest;
import com.ristek.medsosristekbackend.payload.UpdatePostinganRequest;
import com.ristek.medsosristekbackend.payload.UploadPhotoRequest;
import com.ristek.medsosristekbackend.security.services.UserDetailsImpl;
import com.ristek.medsosristekbackend.service.PostinganRestService;
import com.ristek.medsosristekbackend.service.UserMedsosRestService;
import com.ristek.medsosristekbackend.model.UserMedsosModel;
import com.ristek.medsosristekbackend.payload.MessageResponse;
import com.ristek.medsosristekbackend.payload.UpdateCloseFriendRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@RestController
@RequestMapping("/api/user")
public class UserMedsosController {

    @Autowired
    private UserMedsosRestService userService;

    @Autowired
    private PostinganRestService postingService;
    
    @PostMapping(value = "/edit-bio")
    private ResponseEntity<?> editBio(@Valid @RequestBody UpdateBioRequest updateRequest){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
        userService.updateProfilBio(updateRequest.getBio(), userDetails.getUsername());
        return ResponseEntity.ok(new MessageResponse("Bio changed succesfuly"));
    }

    @PostMapping(value = "/upload-photo")
    private ResponseEntity<?> uploadPhoto(@Valid @RequestBody UploadPhotoRequest uploadRequest){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
        userService.addOrUpdateImage(uploadRequest.getPhotoURL(), userDetails.getUsername());
        return ResponseEntity.ok(new MessageResponse("Picture changed succesfuly"));
    }

    @GetMapping(value = "/get-all-exist-user")
    private ResponseEntity<?> getAllExistUser(){
        return ResponseEntity.ok(userService.getAllUser());
    }

    @CrossOrigin
    @GetMapping(value = "/get-all-user")
    private ResponseEntity<?> getAllUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
        String username = userDetails.getUsername();
        return ResponseEntity.ok(userService.getAllUserTaggedCf(username));
    }

    @CrossOrigin
    @PostMapping(value = "/add-close-friend")
    private ResponseEntity<?> addCloseFriends(@Valid @RequestBody UpdateCloseFriendRequest cfReq){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();

        userService.updateCloseFriends(userDetails.getUsername(), cfReq.getListUser());

        return ResponseEntity.ok(userDetails);
    }

    @CrossOrigin
    @GetMapping(value = "/{usernameC}/profile")
    private ResponseEntity<?> getUserProfile(@PathVariable String usernameC){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();

        UserMedsosModel user = userService.getUser(usernameC);

        if(!(usernameC.equals(userDetails.getUsername()) || userService.isCloseFriends(usernameC, userDetails.getUsername()))){

            user.setListPostingan(postingService.getAllOtherPostingan(usernameC, user.getUsername()));
        }

        return ResponseEntity.ok(user);
    }

}

