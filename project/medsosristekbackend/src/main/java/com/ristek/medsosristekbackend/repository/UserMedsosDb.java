package com.ristek.medsosristekbackend.repository;

import com.ristek.medsosristekbackend.model.PostinganModel;
import com.ristek.medsosristekbackend.model.UserMedsosModel;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMedsosDb extends JpaRepository<UserMedsosModel, String>{
    Optional<UserMedsosModel> findByUsername(String username);
    
    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

}
