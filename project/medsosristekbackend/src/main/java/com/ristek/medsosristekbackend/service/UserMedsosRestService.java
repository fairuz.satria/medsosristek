package com.ristek.medsosristekbackend.service;

import java.util.List;
import java.util.Set;
import java.util.HashMap;

import com.ristek.medsosristekbackend.model.PostinganModel;
import com.ristek.medsosristekbackend.model.UserMedsosModel;
import org.springframework.security.core.Authentication;

public interface UserMedsosRestService {
    UserMedsosModel addUser(UserMedsosModel user);

    List<UserMedsosModel> retrieveListUser();

    String encrypt(String password);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    Authentication getAuthentication();

    void setAuthentication(Authentication newAuthentication);

    UserMedsosModel updateProfilBio(String newBio, String username);

    UserMedsosModel addOrUpdateImage(String photoUrl, String username);

    boolean isCloseFriends(String usernameC, String username);

    List<UserMedsosModel> getAllUser();

    void addCloseFriend(String usernameC, UserMedsosModel user);

    UserMedsosModel getUser(String username);

    Boolean updateCloseFriends(String username, List<HashMap<String,String>> objAllUser);

    void unFollow(String usernameC,  UserMedsosModel user);

// List<UserMedsosModel> getAllUserNotCloseFriend(String username);

    List<UserMedsosModel> getAllUserTaggedCf(String username);

    Set<UserMedsosModel> getCloseFriends(String username);

    
}
