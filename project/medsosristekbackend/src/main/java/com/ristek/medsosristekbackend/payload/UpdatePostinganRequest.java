package com.ristek.medsosristekbackend.payload;

import lombok.Data;

@Data
public class UpdatePostinganRequest {
    String isi;
    Long id;
}
