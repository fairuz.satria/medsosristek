package com.ristek.medsosristekbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedsosristekbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedsosristekbackendApplication.class, args);
	}

}
