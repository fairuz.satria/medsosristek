package com.ristek.medsosristekbackend.repository;

import com.ristek.medsosristekbackend.model.PostinganModel;
import com.ristek.medsosristekbackend.model.UserMedsosModel;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostinganDb extends JpaRepository<PostinganModel, Long>{
    Optional<PostinganModel> findById(Long id);

}
